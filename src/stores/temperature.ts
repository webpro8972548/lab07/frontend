import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import http from '@/services/http'
import { useLoadingStore } from './loading'
import temperatureService from '@/services/temperature'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()
  type ReturnData = {
    celsius: number
    fahrenheit: number
  }
  async function callConvert(){
    //result.value  = convert(celsius.value)
    loadingStore.doLoad()
    try{
      console.log(`/temperature/convert/${celsius.value}`);
    //const res = await axios.get(`http://localhost:3000/temperature/convert/${celsius.value}`)
    const res = await http.post(`/temperature/convert`,{
      celsius: celsius.value
    })
    const convertResult = res.data as ReturnData
      console.log(convertResult);
      result.value = convertResult.fahrenheit
    }catch(e){
      console.log(e);
      
    }
    loadingStore.finish()
    console.log('Store: finish call convert')

  }
  

  return { valid,result,celsius,callConvert }
})
